## Intro
This is a test task for Visual Composer. It's purpose is to provide rating plugin for WordPress 4.9.8 with external Laravel service for storing ratings.

## Installation
There are 2 main ways of installing this application - Dockerized app installation and normal installation.

### Dockerized version
1. Clone the repository.
2. If permissions doesn't allow to run develop script, run `sudo chmod +x develop`. Run `./develop up -d`.
3. When services are running, connect to `database` service and run `CREATE DATABASE api IF NOT EXISTS;` to create database for `laravel` (api) service.
4. Optionally, you can do `cp .env.example .env` in order to edit settings for application (ports, image tags etc.), otherwise defaults in `docker-compose.yml` are already provided and used.
5. Run all the necessities for `laravel` container:`cp .env.example` -> `./develop composer install` -> `./develop art key:generate` -> `./develop art migrate`.
6. You can visit the WordPress site on `localhost:<port provided in .env/docker-compose.yml>` and install the site.
7. You can use pre-made posts in order to rate the article and comment. Request will be sent out, stored and response will container average_rating for post which will be stored in the post meta which can be looked up by enabling `Custom fields` in post edit view.

### Normal way
1. Clone the repository.
2. Make a WordPress installation locally.
3. In `plugins` directory you can copy the `rate-my-thoughts.php` named plugin and put it in `<WP work dir>/wp-content/plugins` directory.
4. Visit WP admin panel and you can enable the plugin.
5. Open `api` folder and run `cp .env.example .env` and change the credentials to match your local database credentials.
6. Run `composer install` followed by generating app key and migrations.
7. Change the address for `wp_remote_post` to go to Laravel endpoint `POST /ratings` (not sure if you can store REST API url in WP for plugin namespace).

*Recommended*: use Docker way of installing the app.

## Possible updates outside of test scope
1. When the record is being deleted from the database in Laravel using model deletetion/destroy methods, it will not reflect on front-end (this case, WordPress site). Best solution would be to make a webhook that will POST/PUT to WordPress site which could find the deleted rating and delete it from post meta and recalculate average on front-end.
2. Better way of storing ratings for posts? Not sure if WP equivalent of caching (transients) is a better place for it since storing as post meta would involve serializing the data in order to store it there. It would be great of `update_post_meta` function to have a callable parameter which could pass existing values for meta key which can then present a logic of updating rather than simply overwriting.
3. Object-oriented way of writing a plugin, but complexity was not so significant in order to do that, BUT that would allow me to create a more readable/simple wrapper around WP functions + maybe provide such implementations as in previous bullet point.