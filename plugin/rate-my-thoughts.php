<?php

defined( 'ABSPATH' ) or die( 'No direct access permitted.' );

/**
 * Plugin Name: Rate My Thoughts
 * Description: Add rating along with your comment to post in context.
 * Version. 1.0
 * Requires at least: 4.9.8
 * Requires PHP: 5.6
 * Author: Davis Naglis
 * License: GPL2
 * License URI: https://wwww.gnu.org/licenses/gpl-2.0.html
 */

/**
 * Plugin priority set to low in order for other core functionality
 * to run first, pretty safe priority.
 */
define( 'NOT_IMPORTANT', 99 );

add_action( 'the_post', 'conditionally_add_visual' );

/**
 * Allowing this plugin to work only on posts where comments are allowed.
 */
function conditionally_add_visual( $post ) {
	if ( comments_open( $post->ID ) && should_add_ratings_for_post( $post ) ) {
		add_rating_visual();
	}
}

/**
 * Determine conditions for 'registering' ratings.
 *
 * @param WP_Post $post
 *
 * @return bool
 */
function should_add_ratings_for_post( $post ) {
	return is_user_logged_in()
	       && ! current_user_can( 'subscriber' )
	       && ! has_user_rated_post( $post->ID, (int) get_current_user_id() );
}

/**
 * Determine if current user has already rated current post.
 *
 * @param int $post_id
 * @param int $user_id
 *
 * @return boolean
 */
function has_user_rated_post( $post_id, $user_id ) {
	return in_array( $user_id, get_post_meta( $post_id, 'users_rated', false ) );
}

/**
 * Bread and butter for the plugin. Hooking into functionality.
 */
add_filter( 'preprocess_comment', 'process_the_comment', NOT_IMPORTANT, 1 );

function process_the_comment( $comment ) {
	// Allow to rate the post only 1 time.
	if ( has_user_rated_post( $comment[ 'comment_post_ID' ], $comment[ 'user_id' ] ) ) {
		return $comment;
	}

	// Not providing a rating just allows to
	// post a normal comment.
	if ( ! isset( $_POST[ 'rating' ] ) ) {
		return $comment;
	}

	$request_body = prepare_comment_post_data( $comment );

	$response = wp_remote_post( 'http://laravel:8000/ratings', $request_body );

	// HTTP 400 used for "User already rated the post" message.
	// Primarily dying on HTTP 5xx.
	if ( ! in_array( wp_remote_retrieve_response_code( $response ), [ 200, 201, 400 ] ) ) {
		wp_die( __( 'Error: Failed to save a comment. Try again.' ) );
	}

	add_post_meta( $comment[ 'comment_post_ID' ], 'users_rated', $comment[ 'user_id' ] );
	update_post_meta( $comment[ 'comment_post_ID' ], 'average_rating', get_average_rating( $response ) );

	return $comment;
}

/**
 * Extract average rating from REST API response.
 *
 * @param array $response
 *
 * @return void
 */
function get_average_rating( &$response ) {
	$body = wp_remote_retrieve_body( $response );

	return json_decode( $body )->data->average_rating;
}

/**
 * Set up request body to send to REST API including comment data.
 *
 * @param array $comment
 *
 * @return array
 */
function prepare_comment_post_data( array $comment ) {
	$comment = array_merge( $comment, [ 'rating' => (int) $_POST[ 'rating' ], ] );

	return collect_request_data( $comment );
}

/**
 * Add rating html/css under comment box.
 *
 * @return void
 */
function add_rating_visual() {
	add_action( 'comment_form_logged_in_after', 'show_rating' );
}

function show_rating() {
	echo get_rating_html();
}

/**
 * HTML for the rating bar.
 *
 * @return mixed
 */
function get_rating_html() {
	$wrapper = '<strong>' . __( 'Rate this article' ) . ': </strong>';
	$wrapper .= '<div class="post_rating">';

	for ( $i = 1; $i < 6; $i ++ ) {
		$wrapper .= "{$i}&nbsp;<input type='radio' name='rating' value={$i}>";
	}

	$wrapper .= '<div><br>';

	return $wrapper;
}

/**
 * Collect data to send to external API.
 *
 * @param array $comment
 *
 * @return array
 */
function collect_request_data( $comment ) {
	return array(
		'body' => array(
			'author'    => $comment[ 'comment_author' ],
			'author_id' => $comment[ 'user_id' ],
			'comment'   => $comment[ 'comment_content' ],
			'post_id'   => (int) $comment[ 'comment_post_ID' ],
			'rating'    => $comment[ 'rating' ],
		)
	);
}
