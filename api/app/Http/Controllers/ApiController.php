<?php

namespace App\Http\Controllers;

use App\Http\Resources\RatingResource;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    /**
     * Store rating and return mean rating for post.
     *
     * @param Request $request
     * @return RatingResource
     */
    public function store(Request $request)
    {
        $ratingData = Validator::make($request->all(), [
            'author' => 'required',
            'comment' => 'required',
            'rating' => 'min:1|max:5',
            'author_id' => 'required',
            'post_id' => 'required',
            'comment_id' => 'required',
        ])->validated();

        if ($this->isRatingProvided($ratingData) && Rating::isUnique($ratingData['author_id'], $ratingData['post_id'])) {
            $rating = Rating::create($ratingData);

            return new RatingResource($rating);
        }

        return response()->json([
            'code' => 400,
            'message' => 'User has already rated post.',
        ], 400);
    }

    /**
     * @param array $requestBody
     * @return bool
     */
    private function isRatingProvided(array $requestBody)
    {
        return array_key_exists('rating', $requestBody);
    }
}
