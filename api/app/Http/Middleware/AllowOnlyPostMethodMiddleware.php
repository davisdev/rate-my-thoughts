<?php

namespace App\Http\Middleware;

use Closure;

class AllowOnlyPostMethodMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->isMethod('post')) {
            return response()->json([
                'code' => 405,
                'message' => 'Only POST requests allowed.',
            ], 405);
        }

        return $next($request);
    }
}
