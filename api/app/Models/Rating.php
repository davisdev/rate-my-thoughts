<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'author',
        'comment',
        'rating',
        'author_id',
        'post_id',
    ];

    /**
     * Determine if specified user has already voted on a post.
     *
     * @param int $authorId
     * @param int $postId
     * @return bool
     */
    public static function isUnique(int $authorId, int $postId)
    {
        $authorComments = Rating::where('author_id', $authorId)
            ->where('post_id', $postId)->first();

        return $authorComments === null;
    }

    /**
     * Calculate the average for given post.
     *
     * @param int $id
     * @return mixed
     */
    public function averageForPost(int $id)
    {
        $ratings = Rating::where('post_id', $id)->get();

        return collect($ratings->pluck('rating'))->avg();
    }
}
